package co.com.choucair.certification.pruebatecnica.tasks;

import co.com.choucair.certification.pruebatecnica.userinterface.AddressPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.EnterValue;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class LoginAddress implements Task {
    public static LoginAddress onThePAge() {
        return Tasks.instrumented(LoginAddress.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue("Pasto").into(AddressPage.INPUT_CITY),
                Enter.theValue("520010").into(AddressPage.INPUT_POSTAL_CODE),
                Click.on(AddressPage.BUTTON_NEXT));

    }
}
