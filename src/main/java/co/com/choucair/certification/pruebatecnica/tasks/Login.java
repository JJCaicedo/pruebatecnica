package co.com.choucair.certification.pruebatecnica.tasks;

import co.com.choucair.certification.pruebatecnica.userinterface.RegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Login implements Task {

    public static Login onThePage() {
        return Tasks.instrumented(Login.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo( Enter.theValue("IngresaTuNombre").into(RegisterPage.INPUT_FIRST_NAME),
                Enter.theValue("IngresaTuApellido").into(RegisterPage.INPUT_LAST_NAME),
                Enter.theValue("tuemail@mail.com").into(RegisterPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText("May").from(RegisterPage.INPUT_BIRTH_MONTH),
                SelectFromOptions.byVisibleText("10").from(RegisterPage.INPUT_BIRTH_DAY),
                SelectFromOptions.byVisibleText("2000").from(RegisterPage.INPUT_BIRTH_YEAR),
                //Enter.theValue("Spanish").into(RegisterPage.INPUT_LANGUAGES)
                Click.on(RegisterPage.BUTTON_NEXT));
    }
}
