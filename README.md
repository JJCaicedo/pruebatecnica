# Prueba técnica Choucaur
Prueba técnica realizada en Intellij IDEA par el cargo de Analista de Desarrollo de Software


## Pre-requisitos 📋
Intellij
JAVA 1.8 y JDK
GRADLE
Cucumber


## Automatización 📦
Registro de un usuario en la página Utest

## Autor✒️

Jhonatan Caicedo Ortega - Ing. electrónico

Profundos agredecimientos por la oportunidad de participar en el proceso de selección 🎁
